(eval-when (:compile-toplevel :load-toplevel :execute)
  (require :asdf)
  (asdf:load-system :snark)
  (require :snark))

(in-package "SNARK-USER")

(defun set-up-snark ()
  (initialize)
  (print-options-when-starting nil)
  (print-rows-when-derived nil)
  (print-summary-when-finished nil)
  (print-agenda-when-finished nil)
  (print-final-rows t)
  (use-resolution t)
  (use-paramodulation t))

(defun example-01 ()
  (set-up-snark)
  (prove '(or a (not a))))

(defun example-02 ()
  (set-up-snark)
  (assert '(implies a b))
  (assert '(implies b c))
  (prove '(implies a c)))

;;; (example-02)

(defun example-03 ()
  (set-up-snark)
  (prove '(implies a c)))

;;; (example-03)
