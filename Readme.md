Formale Techniken der Software-Entwicklung
==========================================

Dieses Repository enthält die Folien zur Vorlesung "Formale Techniken
der Software-Entwicklung" im Sommersemester 2015 an der
Ludwig-Maximilians-Universität München.

Die PDF-Dateien kann man aus den LaTeX-Quellen mit

    $ make

generieren.

Dieses Material steht unter der Creative-Commons-Lizenz
Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen
Bedingungen 4.0 International. Um eine Kopie dieser Lizenz zu sehen,
besuchen Sie [die Creative-Commons
Website](http://creativecommons.org/licenses/by-nc-sa/4.0/).

Copyright Informationen für die verwendeten Bilder sind in der Datei
`Images/credits.text`.
