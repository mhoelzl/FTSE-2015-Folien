;;; The following form serves to set up the ASDF system definition
;;; facility and the Snark theorem prover, so that this file can be
;;; compiled and loaded without any additional dependencies.

(eval-when (:compile-toplevel :load-toplevel :execute)
  (require :asdf)
  (asdf:load-system :snark)
  (require :snark)
  (require :snark-runner (merge-pathnames #P"snark-runner.lisp")))

;;; The SNARK-USER package contains all symbols necessary for
;;; interacting with Snark and is useful for defining and
;;; experimenting with simple theories.

(in-package "SNARK-USER")

;;; Definition of the Theory.
;;; ========================

(defun enable-snark-output ()
  "Value of the PRINT-ROWS-WHEN-DERIVED and PRINT-FINAL-ROWS options
 of Snark.  We disable this for non-interactive proofs and enable it
 for interactive proofs."
   (not *print-proof-info*))

(defun define-theory ()
  "Set up Snark for 'logic programming' and define a simple theory
that contains a function symbol CONS and predicates CAR?, CDR?, CONS?
and MEMBER?.  These predicate symbols are logic-programming
equivalents to the corresponding Lisp/ACL2 functions.  Note that CONS
is a purely logical, uninterpreted function symbol that has no
connection to the Lisp function CONS.  The question marks after CAR?,
CDR? CONS? and MEMBER? are just to make clear that these are
predicates; they are not required by Snark and have no semantic
meaning.  We set the ASSERT-SEQUENTIAL option to prevent Snark from
deriving results that are not closely connected to the query formula.
In this mode Snark is no longer complete as theorem prover."
  (snark:with-no-output
    (initialize)
    (use-resolution t)
    ;; (use-paramodulation t)
    (assert-sequential t)
    (assert '(car? (cons ?x ?xs) ?x))
    (assert '(cdr? (cons ?x ?xs) ?xs))
    (assert '(cons? ?x ?xs (cons ?x ?xs)))
    (assert '(member? ?x (cons ?x ?xs)))
    (assert '(implied-by (member? ?x (cons ?y ?xs))
	      (member? ?x ?xs))))
  ;;; Ensure that the printing options of Snark are set after
  ;;; SNARK:WITH-NO-OUTPUT since that macro modifies the PRINT-*
  ;;; options.
  (print-options-when-starting nil)
  (print-rows-when-derived (enable-snark-output))
  (print-summary-when-finished nil)
  (print-agenda-when-finished nil)
  (print-final-rows (enable-snark-output)))


(defun generate-cars-01 ()
  "Show that the CAR? predicate can compute the CAR of a List and that
subsequent calls of (CLOSURE) return with no additional results."
  (define-theory)
  (prove* '(car? (cons a ?xs) ?x)
	  :answer '(the-car-is ?x)))

(add-proof-function 'generate-cars-01)

#+(or)
(print-n-answers 'generate-cars-01)

(defun generate-cars-02 ()
  "Show that the CAR? predicate can compute the CAR of a list even
when it is a variable.  In this case the output variable (?Y) is
unified with the input variable (?X)."
  (define-theory)
  (prove* '(car? (cons ?x ?xs) ?y)
	  :answer '(the-car-is ?x ?y)))

(add-proof-function 'generate-cars-02)

#+(or)
(print-n-answers 'generate-cars-02)

(defun generate-cdrs ()
  "Show that the CDR? predicate can compute the possible CDRs of a
list."
  (define-theory)
  (prove* '(cdr? (cons ?x ?xs) ?ys)
	  :answer '(the-cdr-is ?x ?xs ?ys)))

(add-proof-function 'generate-cdrs)

#+(or)
(print-n-answers 'generate-cdrs)

(defun check-member-01 ()
  "Show that MEMBER? can work as a predicate on ground terms."
  (define-theory)
  (prove* '(member? a (cons a (cons b (cons c nil))))))

(add-proof-function 'check-member-01)

#+(or)
(print-n-answers 'check-member-01)

(defun check-member-02 ()
  "Show that MEMBER? can work as a predicate on ground terms."
  (define-theory)
  (prove* '(member? b (cons a (cons b (cons c nil))))))

(add-proof-function 'check-member-02)

#+(or)
(print-n-answers 'check-member-02)

(defun check-member-03 ()
  "Show that MEMBER? can work as a predicate on ground terms."
  (define-theory)
  (prove* '(member? c (cons a (cons b (cons c nil))))))

(add-proof-function 'check-member-03)

#+(or)
(print-n-answers 'check-member-03)

(defun check-member-04 ()
  "Show that MEMBER? can work as a predicate on ground terms."
  (define-theory)
  (prove* '(member? d (cons a (cons b (cons c nil))))))

(add-proof-function 'check-member-04)

#+(or)
(print-n-answers 'check-member-04)

(defun generate-members-01 ()
  "Show that MEMBER? can generate all possible elements of a list."
  (define-theory)
  (prove* '(member? ?x (cons a (cons b (cons c nil))))
	  :answer '(a-member-is ?x)))

(add-proof-function 'generate-members-01)

#+(or)
(print-n-answers 'generate-members-01)

(defun generate-members-02 ()
  "Show that MEMBER? can generate all possible lists that satisfy an
instance of the MEMBER? predicate."
  (define-theory)
  (prove* '(member? a ?x)
	  :answer '(a-member-is ?x)))

(add-proof-function 'generate-members-02)

#+(or)
(print-n-answers 'generate-members-02)

(defun generate-members-03 ()
  "Show that MEMBER? can generate all possible lists that satisfy an
instance of the MEMBER? predicate."
  (define-theory)
  (prove* '(member? ?x ?y)
	  :answer '(a-member-is ?x ?y)))

(add-proof-function 'generate-members-03)

#+(or)
(print-n-answers 'generate-members-03)

#+(or)
(run)
