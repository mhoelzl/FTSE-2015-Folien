\documentclass[ignorenonframetext]{beamer}

\usecolortheme{crane}

\usepackage{amssymb,amsmath}
\usepackage[utf8]{inputenc}
\usepackage[german]{babel}
\usepackage{turnstile}
\usepackage{fancyvrb}

\input{Macros/macros}

\graphicspath{{Images/}}

\title{Formale Techniken der Software-Entwicklung}
\subtitle{11. Vorlesung}
\author{Matthias Hölzl, Christian Kroiß}

\begin{document}
\frame{\titlepage}


\begin{frame}
  \frametitle{Logik}
  \begin{itemize}
  \item Modelle einer Logik repräsentieren "`Welten"' oder
    "`Universen"' in denen die Axiome der Logik gelten
  \item Die bisher betrachteten Logiken erlauben Aussagen über
    unveränderliche Universen:
    \begin{displaymath}
      \Phi \models \phi
    \end{displaymath}
    in jedem "`Universum"' (jedem Modell) $\M$ in dem alle Aussagen
    aus $\Phi$ gelten, gilt auch $\phi$
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Logik}
  Es ist damit auch möglich Aussagen über funktionale Programme zu
  zeigen, die terminieren und ein Ergebnis zurückgeben:
\begin{verbatim}
(defun my-rev (xs)
  (if (atom xs)
    nil
    (append (my-rev (rest xs)) (list (first xs)))))

(defthm my-rev^2-identity-for-true-listp
  (implies (true-listp xs)
           (equal (my-rev (my-rev xs)) xs)))
\end{verbatim}
\end{frame}

\begin{frame}
  \frametitle{Logik}
  Aber: was ist mit
  \begin{itemize}
  \item \emph{zustandsbehafteten Systemen}, bei denen ein imperatives
    Programm einen Programmzustand manipuliert, oder
  \item potentiell \emph{nichtterminierenden Programmen}, wie z.B
    einer virtuellen Maschine oder einem Betriebssystem, oder
  \item \emph{reaktiven Systemen}, d.h.\ mit Systemen, die kontinuierlich
    auf Ereignisse in ihrer Umwelt interagieren, oder
  \item \emph{nebenläufigen Systemen}, d.h.\ mit Systemen, die aus
    mehreren Komponenten bestehen, die Berechnungen gleichzeitig
    ausführen können?
  \end{itemize}


\end{frame}

\begin{frame}
  \frametitle{Beispiel: Stackmaschine}
  Zu Beginn der Vorlesung haben wir mit der Stackmaschine (SM) ein
  Beispiel gesehen, wie man ein zustandsbehaftetes System in ACL2
  modellieren kann:
  \begin{itemize}
  \item Der Zustand wird explizit als Datenstruktur repräsentiert
  \item Alle Funktionen des Systems bekommen einen Zustand als
    Argument und geben einen (neuen) Zustand als Wert zurück
  \item Bei der betrachteten SM war der komplette Zustand als Stack
    repräsentiert, die Abarbeitung einer einzelnen Anweisung erfolgt
    durch eine Funktion \texttt{sm-step}, die eine Anweisung, eine
    Variablenbelegune und einen Zustand auf einen neuen Zustand
    abbildet.
  \item Die SM führt diese \texttt{sm-step}-Funktion in einer Schleife
    aus und generiert dabei eine Folge von Zuständen
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Beispiel: Stackmaschine}
  Eine vereinfachte Form der Stackmaschine (ohne Variablen) ist:
\begin{verbatim}
(defun sm-step (ins stk)
  (let ((op (first ins)))
    (case op
          (pushc (sm-push (second ins) stk))
          (dup   (sm-push (sm-top stk) stk))
          (add   (sm-push (+ (sm-top (sm-pop stk))
                             (sm-top stk))
                          (sm-pop (sm-pop stk))))
          (mul   (sm-push (* (sm-top (sm-pop stk))
                             (sm-top stk))
                          (sm-pop (sm-pop stk))))
          (t stk))))
\end{verbatim}
\end{frame}

\begin{frame}[fragile,fragile]
  \frametitle{Beispiel: Stackmaschine}
  Die Ausführung eines SM-Programms erhält man durch die Funktion
\begin{verbatim}
(defun sm-run (program stk)
  (if (endp program)
    stk
    (sm-run (rest program)
            (sm-step (first program) stk))))
\end{verbatim}
\end{frame}

\begin{frame}[fragile,fragile]
  \frametitle{Beispiel: Stackmaschine}
  Eine Liste mit den bei der Ausführung eines Programms auftretenden
  Zuständen kann man folgendermaßen berechnen
\begin{verbatim}
(defun sm-states (program stk)
  (if (endp program)
    (list stk)
    (cons stk
          (sm-states (rest program)
                     (sm-step (first program) stk)))))
\end{verbatim}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Beispiel: Stackmaschine}
  \begin{Pvs}
(sm-states '((pushc 1)
             (pushc 2)
             (dup)
             (add)
             (pushc 3)
             (mul))
           '())

\eval (() (1) (2 1) (2 2 1) (4 1) (3 4 1) (12 1))
  \end{Pvs}
\end{frame}

\begin{frame}
  \frametitle{Beispiel: Stackmaschine}
  \begin{itemize}
  \item Die besprochene Stackmaschine kennt keine Iteration oder
    Rekursion, daher ergibt die wiederholte Anwendung der
    \texttt{sm-step}-Funktion durch \texttt{sm-states} eine endliche
    Folge von Zuständen
    
    \vspace*{-3mm}
    \begin{displaymath}
      s_0 \to s_a \to \dots \to s_n
    \end{displaymath}
  \item Im Beispiel ist das die Folge
    \begin{multline*}
      \texttt{()} \to \texttt{(1)} \to \texttt{(2 1)} \to
      \texttt{(2 2 1)} \to \texttt{(4 1)} \to\\
      \to \texttt{(3 4 1)} \to
      \texttt{(12 1)}
    \end{multline*}

  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Beispiel: Stackmaschine}
  \begin{itemize}
  \item Im Allgemeinen kann die Ausführung zustansbehafteter Systeme
    zu einer unendlichen Folge von Zuständen

    \vspace*{-2mm}
    \begin{displaymath}
      s_0 \to s_1 \to s_2 \to \dots 
    \end{displaymath}
    führen.
  \item Das lässt sich aber in den bisher besprochenen Logiken nicht
    direkt modellieren, weil dort alle Funktionen terminieren müssen
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Beispiel: Stackmaschine}
  \begin{itemize}
  \item Damit wir endliche und unendliche Abläufe formal gleich
    behandeln können, setzen wir im Folgenden meistens endliche
    Abläufe mit einem Pseudozustand $s_t$ fort, schreiben also für die
    erste Folge:

    \vspace*{-3mm}
    \begin{displaymath}
      s_0 \to s_a \to \dots \to s_n \to s_t \to s_t \dots
    \end{displaymath}
  \item Für die Stackmaschine erhalten wir so die unendliche Folge von
    Zuständen
    \begin{multline*}
      \texttt{()} \to \texttt{(1)} \to \texttt{(2 1)} \to
      \texttt{(2 2 1)} \to \texttt{(4 1)} \to\\
      \to \texttt{(3 4 1)} \to
      \texttt{(12 1)} \to \texttt{:done} \to \texttt{:done} \to \dots
    \end{multline*}
  wenn wir \texttt{:done} als Pseudozustand $s_t$ verwenden
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Modellierung von Zustandsübergängen der SM}
  \begin{itemize}
  \item Bei der SM gibt es unendlich viele mögliche Zustände, denn ein
    Zustand enthält
    \begin{itemize}
    \item ganze und natürliche Zahlen als Elemente
    \item einen Stack unbegrenzter Größe
    \end{itemize}
  \item Der Übergang von einem Zustand zu seinem Nachfolger ist eine
    Funktion, und damit eindeutig
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Modellierung von Zustandsübergängen der SM}
  \begin{itemize}
  \item Bei der gezeigten SM kann jeder Ablauf nur endlich viele
    Zustände annehmen.
  \item Würde man die SM um Schleifen erweitern, so wären auch
    unendliche Abläufe möglich, die unendlich viele Zustände annehmen.\\[1ex]
    Z.B. würde ein Programm, das in einer Endlosschleife die
    Anweisungen \texttt{((pushc 1) (add))} ausführt zur folgenden
    Folge von Zuständen führen, wenn es mit Stack \texttt{(1)}
    gestartet wird:
\begin{verbatim}

((1) (1 1) (2) (1 2) (3) (1 3) (4) (1 4) ...)
\end{verbatim}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Modellierung von Zustandsübergängen der SM}
  \includegraphics[width=11cm]{Images/vm-01}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Modellierung von Zustandsübergängen der SM}
  \includegraphics[width=11cm]{Images/vm-01a}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Abstraktere Modellierung von Zustandsübergängen der SM}
  Manche Folgen von Instruktionen führen zu identischen Zuständen.
  Wenn wir jeden Zustand nur einmal darstellen wollen erhalten wir
  einen (unendlichen) gerichteten Graphen, keinen Baum:
  
  \vspace*{-6mm}
  \hspace*{-1.4cm}
  \includegraphics[width=14cm]{Images/vm-02}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Abstraktere Modellierung von Zustandsübergängen der SM}
  Wenn wir dabei von den Instruktionen absehen erhalten wir folgenden
  Graphen: 

  \vspace*{-10mm} 
  \hspace*{-1.4cm}
  \includegraphics[width=14cm]{Images/vm-02a}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Abstraktere Modellierung von Zustandsübergängen der SM}
  \begin{itemize}
  \item Auch dieser Graph ist noch unendlich und damit nicht gut für
    die automatische Analyse geeignet.
  \item Bei der Verifikation von Eigenschaften eines Systems sind wir
    oft nur an gewissen Aspekten des Zustandes interessiert.
  \item Wenn wir z.B. die SM in Hardware implementieren, wollen wir
    evtl.\ nur eine begrenze Stacktiefe erlauben.  Dann können wir
    alle Zustände mit der gleichen Stacktiefe identifizieren.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Abstraktere Modellierung von Zustandsübergängen der SM}
\begin{verbatim}
(defun stack-depth (stk)
  (length stk))

(defun sm-stack-depths (program stk)
  (if (endp program)
    (list (stack-depth stk))
    (cons (stack-depth stk)
          (sm-stack-depths
           (rest program)
           (sm-step (first program) stk)))))

(defthm stack-depth-for-single-step
  (<= (stack-depth (sm-step ins stk))
      (1+ (stack-depth stk))))
\end{verbatim}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Abstraktere Modellierung von Zustandsübergängen der SM}
  \begin{Pvs}
(sm-stack-depths '((pushc 1)
                   (pushc 2)
                   (dup)
                   (add)
                   (pushc 3)
                   (mul))
                 '())

\eval (0 1 2 \blue{3} 2 \blue{3} 2)
  \end{Pvs}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Abstraktere Modellierung von Zustandsübergängen der SM}
  Dann erhalten wir einen wesentlich einfacheren Graphen:

  \includegraphics[width=12cm]{Images/vm-03}

  (In den Zuständen ist die Tiefe des Stacks angegeben, also der Wert
  von \texttt{stack-depth}.)
\end{frame}

\begin{frame}[fragile]
  \frametitle{Abstraktere Modellierung von Zustandsübergängen der SM}
  Wir können dann weiter abstrahieren und nur Kanten betrachten,
  die die Tiefe des Stacks verändern:

  \includegraphics[width=12cm]{Images/vm-03a}

  Dieser Graph ist immer noch unendlich.
\end{frame}

\begin{frame}
  \frametitle{Abstraktere Modellierung von Zustandsübergängen der SM}
  Wenn wir in der Hardware nur Speicherplatz für zwei Stackframes
  bereitstellen, können wir das Modell nochmals vereinfachen:

  \includegraphics[width=12cm]{Images/vm-03b}

  Dieser Graph hat nur noch endlich viele Zustände.
\end{frame}


\begin{frame}
  \frametitle{Abstraktere Modellierung von Zustandsübergängen der SM}
  Wenn wir die Aussagenlogischen Formeln $A_0$ ($A_1, A_2$) als "`Die
  Tiefe des Stacks ist $0$"' ($1, 2$) und $A_\bot$ als "`Die zulässige
  Stacktiefe wurde überschritten"' definieren, dann können wir jedem
  Zustand (genau) eine dieser Formeln zuordnen.  Wenn wir noch andere
  Eigenschaften betrachten (z.B., $A_g = $ "`Das Programm hat die
  gültige Stacktiefe nicht überschritten"') können wir jedem Knoten
  des Graphen eine Menge von atomaren Formeln zuordnen, die in diesem
  Zustand gilt:

  \includegraphics[width=12cm]{Images/vm-03c}

  Einen gerichteten Graphen zusammen mit einer solchen Zuordnung
  nennen wir ein \emph{Transitionssystem.}
\end{frame}

\begin{frame}
  \frametitle{Abstraktere Modellierung von Zustandsübergängen der SM}
  Jedem Zustand entspricht also eine aussagenlogisches Modell (d.h.\
  eine Variablenbelegung), die Transitionen entsprechen Übergängen
  zwischen Modellen.

  \includegraphics[width=12cm]{Images/vm-03c}
\end{frame}

\begin{frame}
  \frametitle{Auffalten eines Transitionssystems}
  Alle möglichen Durchläufe durch ein Transitionssystem lassen sich
  in einem (unendlichen) Baum darstellen:

  \vspace*{-10mm}
  \hspace*{-8mm}
  \includegraphics[width=12cm]{Images/vm-03c}

  %\vspace*{-5mm}
  %ergibt den Baum

  \vspace*{-1.2cm}
  \hspace*{-10mm}
  \includegraphics[width=13.3cm]{Images/vm-04}
\end{frame}

\begin{frame}
  \frametitle{Auffalten eines Transitionssystems}
  Jeder Ast in einem derartigen Baum entspricht einem Durchlauf durch
  das Transitionssystem.  Wenn wir einen Knoten in diesem Baum
  auswählen (der ja einen Zustand $s$ im zugrundeliegenden
  Transitionssystem entspricht, und dem dadurch ein Modell zugeordnet
  ist), können wir z.B. fragen:
  \begin{itemize}
  \item Gilt eine Aussage $A$ in $s$?
  \item Gilt eine Aussage $A$ im unmittelbar auf $s$ folgenden
    Zustand?
  \item Gilt $A$ in allen Durchläufen, die mit $s$ beginnen?
  \item Gibt es einen mit $s$ beginnenden Durchlauf $\pi$, so dass $A$
    in allen Knoten von $\pi$ gilt?
  \item Gibt es einen mit $s$ beginnenden Durchlauf, in dem $A$
    irgendwann gilt?
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Temporallogik}
  Eine Temporallogik ist eine Logik, in der man Operatoren hat, die
  derartige "`zeitlichen"' Zusammenhänge beschreiben.  Dabei
  unterscheidet man Logiken, mit
  \begin{itemize}
  \item linearer Zeit: diese Temporallogiken beschränken sich auf
    Fragen, die einen zukünftigen Ablauf, bzw.\ alle in einem Knoten
    beginnenden Abläufe betreffen, und solche mit
  \item verzweigter Zeit: in diesen Logiken kann man über zukünftige
    Abläufe existenziell und universell quantifizieren.
  \end{itemize}
  Wir werden im Folgenden die Logik CTL* (Computation Tree Logic*)
  betrachten, der ein verzweigtes Modell der Zeit zugrundeliegt.
\end{frame}

\begin{frame}
  \frametitle{Syntax von CTL*}
  In CTL* werden die Aussagenlogischen Formeln erweitert um
  \begin{itemize}
  \item Die Pfadquantoren $\ctlA$ (für alle Pfade) und $\ctlE$ (es
    gibt einen Pfad)
  \item Die Temporaloperatoren
    \begin{itemize}
    \item $\ctlX \phi$ (neXt time): die Eigenschaft $\phi$ gilt im
      nächsten Zustand des Pfades
    \item $\ctlF \phi$ (Future): die Eigenschaft $\phi$ wird auf einem
      Zustand des Pfades gelten
    \item $\ctlG \phi$ (Globally): die Eigenschaft $\phi$ gilt für jeden
      Zustand des Pfades
    \item $\phi \ctlU \psi$ (Until): gilt, wenn es einen Zustand auf
      dem Pfad gibt, für den $\psi$ gilt, und wenn in allen
      vorhergehenden Zuständen $\phi$ gilt
    \item $\phi \ctlR \psi$ (Release): gilt, wenn $\psi$ bis zum
      ersten Zustand gilt, in dem $\phi$ gilt (einschließlich dieses
      Zustandes).  $\phi$ muss aber nicht gelten.
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Beispiele (1)}
  \begin{itemize}
  \item Es ist unmöglich in einen Zustand zu kommen, in dem
    \texttt{started} gilt, aber nicht \texttt{ready}: $\ctlG
    \lnot(\mathtt{started} \land \mathtt{ready})$
  \item Jede Anfrage wird (irgendwann) beantwortet:
    $\ctlG(\mathtt{requested} \imp \ctlF \mathtt{acknowledged})$
  \item Ein Prozess wird in jedem Pfad unendlich oft freigeschaltet:
    $\ctlA\ctlG \ctlF \mathtt{enabled}$
  \item Es gibt einen Pfad, auf dem der Prozess deadlockt: $\ctlE
    \ctlF \ctlG \mathtt{deadlock}$
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Beispiele (2)}
  \begin{itemize}
  \item Wenn ein Prozess unendlich oft aktiviert wird, dann startet er
    unendlich oft: $\ctlG \ctlF \mathtt{enabled} \imp \ctlG \ctlF
    \mathtt{start}$
  \item Ein Lift, der vom 2. Stock aus nach oben in den 5. Stock fährt
    ändert seine Richtung nicht: 
    \vspace*{-3mm}
    \begin{multline*}
      \ctlG(\mathtt{floor}(2) \land \mathtt{direction}(\mathtt{up})
      \land \mathtt{buttonPressed}(\mathtt{up})\\
      \imp (\mathtt{direction}(\mathtt{up}) \ctlU \mathtt{floor}(5)))
    \end{multline*}

  \item Ein System kann von jedem Zustand aus neu gestartet werden: $\ctlG
    \ctlE \mathtt{restart}$
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Syntax von CTL*}
  In CTL* gibt es \emph{Zustandsformeln (state formulas)} und
  \emph{Pfadformeln (path formulas)}.  Zustands- und Pfadformeln
  werden auf unterschiedliche Arten interpretiert:
  \begin{itemize}
  \item Jede Zustandsformel wird in einem einzelnen Zustand
    interpretiert
  \item Jede Pfadformel wird entlang eines Pfades interpretiert
  \end{itemize}
  Die Sprache von CTL* ist die Menge der durch die auf den folgenden
  Folien definierten Regeln generierten Zustandsformeln.  Wir
  schreiben $ZF(\phi)$ wenn $\phi$ eine Zustandsformel ist und
  $PF(\phi)$ wenn $\phi$ eine Pfadformel ist.
\end{frame}

\begin{frame}
  \frametitle{Zustandsformeln}
  Sei $AP$ die Menge der atomaren Aussagen.  \emph{Zustandsformeln}
  sind folgendermaßen rekursiv definiert:
  \begin{itemize}
  \item Ist $A \in AP$, dann ist $A$ eine Zustandsformel
  \item Sind $\phi$ und $\psi$ Zustandsformeln, so sind auch $\lnot
    \phi$, $\phi \land \psi$ und $\phi \lor \psi$ Zustandsformeln
  \item Ist $\phi$ eine Pfadformel, dann sind $\ctlA \phi$ und $\ctlE
    \phi$ Zustandsformeln
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Pfadformeln}
  \emph{Pfadformeln} sind folgendermaßen rekursiv definiert:
  \begin{itemize}
  \item Ist $\phi$ eine Zustandsformel, dann ist $\phi$ auch eine
    Pfadformel
  \item Sind $\phi$ und $\psi$ Pfadformeln, dann sind auch $\lnot
    \phi$, $\phi \land \psi$, $\phi \lor \psi$, $\ctlX \phi$, $\ctlF
    \phi$, $\ctlG \phi$, $\phi \ctlU \psi$ und $\phi \ctlR \psi$
    Pfadformeln
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Transitionssysteme und Kripke-Strukturen}
  Sei $\Lang$ eine logische Sprache mit Atomen $AP$.

  \begin{definition}[Transitionssystem]
    Ein Transitionssystem $\M = (S, \to, L)$ besteht aus
    einer Menge von Zuständen $S$, einer binären Relation $\to$ auf
    $S$, so dass es für jedes $s \in S$ ein Element $s' \in S$ gibt,
    für das $s \to s'$ gilt, und einer Funktion $L: S \to \P(A)$, die
    jedem Zustand $s \in S$ die Menge der Atome zuordnet, die in $s$
    gelten.
  \end{definition}

  Die angegebene Definition von Trasitionssystemen ist hauptsächlich
  in der Literatur über Model-Checking verbreitet.

  In anderen Bereichen der Mathematik oder Informatik bezeichnet man
  oft ein Paar $(S, \to)$ mit $\to \subseteq S \times S$ (ohne weitere
  Einschränkungen) als Transitionssystem.
\end{frame}

\begin{frame}
  \frametitle{Kripke-Strukturen}
  \begin{definition}
    Ein Transitionssystem $\M$ zusammen mit einer Menge
    $S_0 \subseteq S$ nennt man eine \emph{Kripke-Struktur}.
  \end{definition}
  Die Menge $S_0$ beinhaltet die möglichen Initialzustände für
  Interpretationen.
\end{frame}

\begin{frame}
  \frametitle{Pfade}
  \begin{definition}[Pfad]
    Ein \emph{Pfad} in $\M$ ist eine unendliche Folge von Zuständen,
    $\pi = s_0, s_1, \dots$, so dass für alle $i$ gilt
    $s_i \to s_{i+1}$.  (Ein Pfad ist somit ein unendlicher Ast in dem
    vom Transitionssystem erzeugten Baum.)
  \end{definition}
\end{frame}

\begin{frame}
  \frametitle{Transitionssysteme und Kripke-Strukturen}
  \begin{itemize}
  \item Wir schreiben $\pi^{s_i}$ oder $\pi^i$ für den Suffix von
    $\pi$, der mit dem Zustand $s_i$ beginnt; wir schreiben auch
    $\pi^s$ um einen Pfad zu benennen, der mit dem Zustand $s$ beginnt
  \item Für eine Zustandsformel $\phi$ schreiben wir
    $\M, s \models \phi$ oder $\sem{\phi}s$ wenn $\phi$ für den
    Zustand $s$ im Transitionssystem $\M$ gilt
  \item Für eine Pfadformel $\phi$ schreiben wir $M, \pi \models \phi$
    oder $\sem{\phi} \pi$, wenn $\phi$ entlang des Pfades $\pi$ im
    Transitionssystem $\M$ gilt
  \item Üblicherweise schreiben wir $\M$ nicht explizit hin, da das
    Transitionssystem meist aus dem Kontext klar ist.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Transitionssysteme und Kripke-Strukturen}
  Wenn $AP$ eine Teilmenge der aussagenlogischen Variablen (oder eine
  Menge bestehend aus prädikatenlogischen Grundatomen) ist, dann
  identifiziert man $s$ wie üblich mit der Menge der Atome, für die
  $s$ wahr ist, bzw.\ mit der characteristischen Funktion dieser Menge
  \begin{align*}
    s \sim \{A_0, \dots, A_n\} \iff\, &s(A_0) = \dots = s(A_n) = \true \text{ und
    }\\& s(B) = \false \text{ sonst.}
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Transitionssystem und Kripke-Strukturen}
  Falls ein Zustand eindeutig durch die Werte der aussagenlogischen
  Variablen des Systems bestimmt ist und die Zustandsübergänge
  $s \to s'$ deterministisch sind, kann man die Zustandsübergänge
  eines Systems in einer \emph{faktorisierten} Form schreiben: Man
  gibt für jede Variable $A_i$ an, wie man den Wert, den $A_i$ nach
  einer Transition $s \to s'$ hat, aus den Werten der Variablen in $s$
  berechnen kann.
  \begin{align*}
    s'(A_1) &= f_1(A_1, \dots, A_n)\\
    &\hspace{2mm}\vdots\\
    s'(A_n) &= f_n(A_1, \dots, A_n)
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Beispiel: Synchroner Zähler Modulo 8}
  \center
  \includegraphics[width=6cm]{Images/counter-mod8}
\end{frame}

\begin{frame}
  \frametitle{Beispiel: Synchroner Zähler Modulo 8}
  \includegraphics[width=3.8cm]{Images/counter-mod8}
  \vspace*{-4cm}
  \begin{align*}
    \intertext{\hspace*{4.7cm}Zustände: Belegungen für $A_0, A_1, A_2$}
    \hspace*{2cm} A_0' &= \lnot A_0\\
    A_1' &= A_0 \oplus A_1\\
    A_2' &= (A_0 \land A_1) \oplus A_2
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Beispiel: Synchroner Zähler Modulo 8}
  \includegraphics[width=4cm]{Images/counter-mod8}
  \vspace*{-4cm}
  \begin{align*}
    \intertext{\hspace*{4.5cm}Zustände: Belegungen für $A_0, A_1, A_2$}
    \hspace*{2cm} A_0' &= \lnot A_0\\
    A_1' &= A_0 \oplus A_1\\
    A_2' &= (A_0 \land A_1) \oplus A_2\\[1ex]
\intertext{\hspace*{4.5cm}$s\to s'$ genau dann, wenn}
    \hspace*{3.5cm} s'(A_0) &= \sem{\lnot A_0}s\\
    s'(A_1) &= \sem{A_0 \oplus A_1}s\\
    s'(A_2) &= \sem{(A_0 \land A_1) \oplus A_2}s\\
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Transitionssystem und Kripke-Strukturen}
  Oft verwendet man die faktorisierte Schreibweise
  \begin{align*}
    s'(A_1) &= f_1(A_1, \dots, A_n)\\
    &\hspace{2mm}\vdots\\
    s'(A_n) &= f_n(A_1, \dots, A_n)
  \end{align*}
  auch dann, wenn $s \to s'$ keine Funktion ist. In diesem Fall
  schreibt man die $f_i$ als "`nichtdeterministische"' Funktionen,
  d.h.\ man führt eine Syntax ein, mit der man ausdrücken kann, dass
  $f_i$ einen aus mehreren möglichen Werten zurückgibt.  Das ist
  z.B. beim später besprochenen NuSMV Model Checker der Fall.

  (Formal betrachtet ist $f_i$ in diesem Fall keine Funktion und
  $s'(A_1) = f_1(A_1, \dots, A_n)$ ist dann nur eine andere
  Schreibweise für eine Relation $R_i(A_1, \dots, A_n, s'(A_1))$.)
\end{frame}

\begin{frame}
  \frametitle{Semantik von CTL*}
  \begin{align*}
    \M, s \models A &\iff p \in L(s)\\
    \M, s \models \lnot\phi &\iff \M, s \not \models \phi\\
    \M, s \models \phi \lor \psi &\iff \M,s \models \phi \text{ oder }
    \M, s \models \psi\\
    \M, s \models \phi \land \psi &\iff \M,s \models \phi \text{ und }
    \M, s \models \psi\\
    \M, s \models \ctlE \phi &\iff \text{Es gibt einen Pfad $\pi^s$ mit
    } \M, \pi \models \phi\\
    \M, s \models \ctlA \phi &\iff \text{Für alle Pfade $\pi^s$ gilt
    } \M, \pi \models \phi
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Semantik von CTL*}
  \begin{align*}
    \M, \pi \models \phi &\iff \text{$ZF(\phi)$, $\pi$ beginnt mit $s$
      und } \M, s \models \phi\\
    \M, \pi \models \lnot\phi &\iff \M, \pi \not \models \phi\\
    \M, \pi \models \phi \lor \psi &\iff \M,\pi \models \phi \text{ oder }
    \M, \pi \models \psi\\
    \M, \pi \models \phi \land \psi &\iff \M,\pi \models \phi \text{ und }
    \M, \pi \models \psi\\
    \M, \pi \models \ctlX \phi &\iff \M, \pi^1 \models \phi\\
    \M, \pi \models \ctlF \phi &\iff \text{es gibt $k$ mit } 
    \M, \pi^k \models \phi\\
    \M, \pi \models \ctlG \phi &\iff \text{für alle $i \geq 0$ gilt } 
    \M, \pi^i \models \phi\\
    \M, \pi \models \phi \ctlU \psi &\iff \exists k\geq 0. \M\pi^k
    \models \psi \land \forall 0 \leq i < k.    \M, \pi^i \models
    \phi\\
    \M, \pi \models \phi \ctlR \psi &\iff \forall j \geq 0. 
    \text{ If } \forall i < j. \M, \pi^i \not\models \phi 
    \text{ then } \M, \pi^j \models \psi
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Beispiel: Wechselseitiger Ausschluss}
  Mehrere Prozesse greifen auf eine gemeinsame Ressource zu.  Dabei
  sollen folgende Eigenschaften garantiert werden:
  \begin{itemize}
  \item \emph{Sicherheit (Safety):} Nur ein Prozess ist im kritischen
    Bereich
  \item \emph{Liveness:} Wenn ein Prozess verlangt in den kritischen
    Bereich zu kommen, kann er das irgendwann
  \item \emph{Nicht-blockierend:} Ein Prozess kann immer verlangen in
    den kritischen Bereich zu kommen
  \item \emph{Nicht-strikt-sequenziell:} Die Prozesse müssen nicht
    streng abwechselnd in den kritischen Bereich
  \end{itemize}
  Wir müssen ein Protokoll finden, das den Wechsel in den kritischen
  Bereich kontrolliert und diese Eigenschaften sicherstellt.
\end{frame}

\begin{frame}
  \frametitle{Beispiel: Wechselseitiger Aussschluss}
  Wir modellieren jeden Prozess durch folgendes Diagramm:
  
  \center
  \includegraphics[width=8cm]{Images/me}

  \begin{itemize}
  \item $n$: Nicht im kritischen Bereich
  \item $t$: Versucht in den kritischen Bereich zu kommen
    \emph{(trying)}
  \item $c$: Im kritischen Bereich
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Beispiel: Wechselseitiger Ausschluss}
  \begin{itemize}
  \item Zwei Prozesse
  \item Jeder Prozess führt die beschriebenen Zustandsübergänge
    zwischen $c$, $t$ und $n$ durch
  \item Die Prozesse werden durch Verschränkung
    \emph{("`Interleaving"')} kombiniert
  \item Der Scheduler wird nicht spezifiziert
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Beispiel: Wechselseitiger Ausschluss}
  Eine Möglichkeit die Verschränkung der beiden Prozesse zu
  modellieren ist in folgendem Diagramm dargestellt:
  
  \input{Images/hr-fig-3-7}
\end{frame}

\begin{frame}
  \frametitle{Beispiel: Wechselseitiger Ausschluss}
  \begin{itemize}
  \item \emph{Safety:} Nur ein Prozess ist im kritischen
    Bereich: $\ctlA \ctlG \lnot(c_1 \land c_2)$
  \item \emph{Liveness:} Wenn ein Prozess verlangt in den kritischen
    Bereich zu kommen, kann er das irgendwann: $\ctlA \ctlG (t_i \imp
    \ctlF c_i)$
  \item \emph{Nicht-blockierend:} Ein Prozess kann immer verlangen in
    den kritischen Bereich zu kommen: $\ctlA \ctlG (n_i \imp \ctlE \ctlX
    t_i)$
  \item \emph{Nicht-strikt sequenziell} Die Prozesse müssen nicht
    streng abwechselnd in den kritischen Bereich: $\ctlE \ctlF(c_1
    \land \ctlE(c_1 \ctlU (\lnot c_1 \land \ctlE (\lnot c_2 \ctlU
    c_1))))$
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Beispiel: Wechselseitiger Ausschluss}
  Eine zweite Modellierungsvariante für wechselseitigen Ausschluss
  ist:
  
  \input{Images/hr-fig-3-8}
\end{frame}

\begin{frame}
  \frametitle{Beispiel: Wechselseitiger Ausschluss}
  \begin{itemize}
  \item In dieser Variante gibt es zwei Zustände ($s_3$ und $s_9$), in
    denen beide Prozesse versuchen in den kritischen Bereich zu
    kommen.
  \item Im Gegensatz zur ersten Modellierung ist immer der Prozess
    damit erfolgreich, der zuerst versucht hat in den kritischen
    Bereich zu kommen.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Einschränkungen: LTL und CTL}
  CTL* erlaubt die Beschreibung vieler Systemeigenschaften, aber
  Model-Checking von CTL*-Spezifikationen ist relativ
  resourcenintensiv.  Man verwendet daher in der Praxis oft
  Temporallogiken, die die Ausdrucksstärke von CTL* einschränken.  Die
  bekanntesten sind CTL und LTL:
  \begin{itemize}
  \item CTL (Computation-Tree Logic): Jeder der Operatoren $\ctlX$,
    $\ctlF$, $\ctlG$, $\ctlU$ und $\ctlR$ muss unmittelbar nach einem
    Pfadquantor stehen.  CTL ist eine Sprache, die nur verzweigte Zeit
    beinhaltet
  \item LTL (Linear-Time Logic): Nur Formeln der Form $\ctlA \phi$ mit
    einer Pfadformel $\phi$, in der alle Zustands-Teilformeln atomar
    sind.  (Typischerweise wird der Allquantor weggelassen.)  LTL ist
    eine Sprache, die nur lineare Zeit beinhaltet
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Beispiel: Wechselseitiger Aussschluss}
  \begin{itemize}
  \item \emph{Safety:} Nur ein Prozess ist im kritischen
    Bereich: $\ctlA \ctlG \lnot(c_1 \land c_2)$  (LTL, CTL)
  \item \emph{Liveness:} Wenn ein Prozess verlangt in den kritischen
    Bereich zu kommen, kann er das irgendwann: $\ctlA \ctlG (t_i \imp
    \ctlF c_i)$ (LTL)
  \item \emph{Nicht-blockierend:} Ein Prozess kann immer verlangen in
    den kritischen Bereich zu kommen: $\ctlA \ctlG (n_i \imp \ctlE \ctlX
    t_i)$ (CTL)
  \item \emph{Nicht-strikt sequenziell} Die Prozesse müssen nicht
    streng abwechselnd in den kritischen Bereich: $\ctlE \ctlF(c_1
    \land \ctlE(c_1 \ctlU (\lnot c_1 \land \ctlE (\lnot c_2 \ctlU
    c_1))))$ (CTL)
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Model Checking}
  Sei $\M$ eine Kripke-Struktur, die ein (nebenläufiges System) mit
  endlich vielen Zuständen beschreibt, und sei $\phi$ eine
  temporallogische Formel, die eine gewünschte Eigenschaft des Systems
  beschreibt.
  \begin{itemize}
  \item Finde alle Zustände für die gilt $\M, s \models \phi$, also
    $MS = \{s \in S \mid M, s \models \phi\}$
  \item Damit die Eigenschaft für das System erfüllt ist, muss gelten
    $S_0 \subseteq MS$
  \item Wenn die Eigenschaft nicht erfüllt ist kann ein Pfad angegeben
    werden, der die Eigenschaft nicht erfüllt.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Vorteile/Nachteile des Model Checkings}
  \begin{itemize}
  \item Model-Checking ist weitgehend automatisch
  \item Model-Checking kann zur Verifikation einzelner Eigenschaften
    hergenommen werden; ein vollständiges Systemmodell ist nicht
    notwendig
  \item Model-Checking eignet sich für nebenläufige und reaktive
    Systeme
  \item Gilt eine Eigenschaft nicht, so können Model-Checker
    typischerweise Fehler-Traces ausgeben
  \item Problem: Explosion des Zustandsraums \emph{(State Explosion)}
  \item Model-Checking funktioniert nicht für datenabhängige Systeme
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{NuSMV}
  \begin{itemize}
  \item Model-Checker für LTL und CTL
  \item Home-Page: \url{http://nusmv.fbk.eu/index.html}
  \item BDD- und SAT-basiertes Model Checking
  \item Heuristiken um die Explosion des Zustandsraums einzudämmen
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{NuSMV}
  \begin{itemize}
  \item Eingabe: Datei mit
    \begin{itemize}
    \item Modell
    \item Temporallogische Formeln, die verifiziert werden sollen
    \end{itemize}
  \item Ausgabe:
    \begin{itemize}
    \item \texttt{true} oder
    \item Beispieltrace, der zeigt, warum eine der Formeln für das
      Modell nicht zutrifft
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{NuSMV Programme}
  \begin{itemize}
  \item NuSVM Programme sind in Module unterteilt
  \item Module können mit Variablen parametrisiert sein
  \item In Modulen werden Variablen deklariert und ihnen Werte
    zugewiesen:
    \begin{itemize}
    \item \texttt{init(v)}: Anfangswert für die Variable \texttt{v}
    \item \texttt{next(v)}: Zuweisung eines neuen Wertes; der Wert
      wird typischerweise aus den aktuellen Werten der Variablen
      berechnet
    \item Die Zuweisung eines Wertes kann nichtdeterministisch sein
    \end{itemize}
  \item Analyse startet mit dem \texttt{main}-Modul
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{NuSMV: Beispiel}
\begin{verbatim}
MODULE main
VAR
  request : boolean;
  state : {ready, busy};
ASSIGN
  init(state) := ready;
  next(state) := case
                   state = ready & request: busy;
                   TRUE : {ready,busy};
                 esac;
SPEC
  AG(request -> AF(state = busy))
\end{verbatim}
\end{frame}

\begin{frame}
  \frametitle{Erinnerung: Wechselseitiger Aussschluss}
  Wir modellieren jeden Prozess durch folgendes Diagramm:
  
  \center
  \includegraphics[width=8cm]{Images/me}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Wechselseitiger Ausschluss in NuSMV}
\footnotesize
\begin{verbatim}
MODULE prc(other-st, turn, myturn)
   VAR
      st: {n, t, c};
   ASSIGN
      init(st) := n;
      next(st) :=
         case
            (st = n)                                   : {t,n};
            (st = t) & (other-st = n)                  : c;
            (st = t) & (other-st = t) & (turn = myturn): c;
            (st = c)                                   : {c,n};
            TRUE                                       : st;
         esac;
      next(turn) :=
         case
            turn = myturn & st = c : !turn;
            TRUE                   : turn;
         esac;
   FAIRNESS running
   FAIRNESS  !(st = c)
\end{verbatim}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Wechselseitiger Ausschluss in NuSMV}
\begin{verbatim}
next(st) :=
   case
      (st = n)                                   : {t,n};
      (st = t) & (other-st = n)                  : c;
      (st = t) & (other-st = t) & (turn = myturn): c;
      (st = c)                                   : {c,n};
      TRUE                                       : st;
   esac;
\end{verbatim}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Wechselseitiger Ausschluss in NuSMV}
\footnotesize
\begin{verbatim}
MODULE main
   VAR
      pr1: process prc(pr2.st, turn, FALSE);
      pr2: process prc(pr1.st, turn, TRUE);
      turn: boolean;
   ASSIGN
      init(turn) := FALSE;
   -- safety
   LTLSPEC  G!((pr1.st = c) & (pr2.st = c))
   -- liveness
   LTLSPEC  G((pr1.st = t) -> F (pr1.st = c))
   LTLSPEC  G((pr2.st = t) -> F (pr2.st = c))
   -- `Negation' von "nicht-strikt-sequenziell" (soll falsch sein)
   LTLSPEC G(pr1.st=c -> 
             ( G pr1.st=c 
           | ( pr1.st=c U (!(pr1.st=c) 
                           & G !(pr1.st=c) 
                           | ((!(pr1.st=c)) U pr2.st=c)))))
\end{verbatim}
\end{frame}

\begin{frame}
  \frametitle{Wechselseitiger Ausschluss in NuSMV}
  Das Programm entspricht folgendem Transitionssystem:

  \bigskip
  \includegraphics[width=11cm]{Images/hr-fig-3-11-crop}
\end{frame}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
