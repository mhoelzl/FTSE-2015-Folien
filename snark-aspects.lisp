;;; The following form serves to set up the ASDF system definition
;;; facility and the Snark theorem prover, so that this file can be
;;; compiled and loaded without any additional dependencies.

(eval-when (:compile-toplevel :load-toplevel :execute)
  (require :asdf)
  (asdf:load-system :snark)
  (require :snark))

(in-package "SNARK-USER")

;;; Some variables that determine which output options of Snark should
;;; be enabled.  This seems somewhat redundant, but since I make
;;; liberal use of SNARK:WITH-NO-OUTPUT which modifies the values of
;;; the display parameters, I want a convenient way to set them after
;;; initialization.

(defparameter *print-rows-when-derived* t)
(defparameter *print-rows-when-finished* nil)
(defparameter *print-agenda-when-finished* nil)
(defparameter *print-final-rows* t)
(defparameter *print-row-answers* t)

(defun set-snark-default-display-parameters ()
  "Set the default parameters for Snark output to the values also set
by SET-SNARK-DISPLAY-PARAMETERS.  Not necessary to run the provided
functions, but useful for interactive experiments."
  (default-print-final-rows *print-final-rows*)
  (default-print-row-answers *print-row-answers*)
  (default-print-options-when-starting nil)
  (default-print-summary-when-finished nil)
  (default-print-rows-when-derived *print-rows-when-derived*)
  (default-print-rows-when-finished *print-rows-when-finished*)
  (default-print-agenda-when-finished *print-agenda-when-finished*)
  (default-print-assertion-analysis-notes nil))

(set-snark-default-display-parameters)

(defun set-snark-display-parameters ()
  "Set the Snark parameters that influence its output."
  (print-final-rows *print-final-rows*)
  (print-row-answers *print-row-answers*)
  (print-options-when-starting nil)
  (print-summary-when-finished nil)
  (print-rows-when-derived *print-rows-when-derived*)
  (print-rows-when-finished *print-rows-when-finished*)
  (print-agenda-when-finished *print-agenda-when-finished*)
  (print-assertion-analysis-notes nil))

(defparameter *use-only-resolution-and-paramodulation* nil)

(defun set-up-snark ()
  (snark:with-no-output
    (initialize))
  (use-resolution t)
  (use-paramodulation t)
  (use-constructive-answer-restriction t)

  ;; Would probably be sufficient for this example
  ;; (use-paramodulation-only-from-units t)
  (unless *use-only-resolution-and-paramodulation*
    (use-hyperresolution t)
    (use-ur-resolution t)
  
    ;; Define a relatively standard term ordering strategy.
    (use-term-ordering :recursive-path)
    (use-default-ordering t)
    (use-literal-ordering-with-resolution 'literal-ordering-p)
    (use-literal-ordering-with-hyperresolution 'literal-ordering-p)
    (use-literal-ordering-with-ur-resolution 'literal-ordering-p)
    (use-literal-ordering-with-paramodulation 'literal-ordering-p)
    (ordering-functions>constants t))
    ;; (use-conditional-answer-creation t)
  
  (set-snark-display-parameters))

;;; The Aspects.
;;; ===========

;;; We define two aspects for a "wizard" state machine similar to the
;;; one shown in the paper "HiLA: Semantic Aspects for UML State
;;; Machines" by Zhang and Hölzl.  The pointcut for these aspects is
;;; the transition between states Spell and Fight.

;;; The first aspect prevents the wizard from entering an enchanted
;;; state if she does not have enough power.

;;; ASPECT1
;;; -->  enough-power --> goto enchanted
;;; |-> ~enough-power --> goto spell

;;; The second aspect forces the wizard to pass a probabilistic stat
;;; check (to "gamble") for an enchantment.  If the player cannot even
;;; initiate the check she stays in her current state.  If she gambles
;;; and wins she enters the enchanted state, otherwise she returns to
;;; the power-up state.  (It would probably make more sense to have a
;;; non-enchanted fight state, but whatever).

;;; ASPECT2
;;; -->  become-invisible -->  can-hide --> goto enchanted
;;; |    |----------- ------> ~can-hide --> goto power-up
;;; |-- ~become-invisible ----------------> goto spell

;;; We define several different ways of modeling these aspects that
;;; can (mostly) be independently enabled or disabled.

(defun set-up-base-theory ()
  "Declare the logical theory for the aspect system."
  (declare-sort 'state)
  (declare-sort 'situation)
  (declare-sorts-incompatible 'state 'situation)
  (declare-constant 'spell :sort 'state)
  (declare-constant 'enchanted :sort 'state)
  (declare-constant 'power-up :sort 'state)
  (declare-relation 'aspect1 2 :sort '((1 situation) (2 state)))
  (declare-relation 'aspect2 2 :sort '((1 situation) (2 state)))
  (declare-relation 'enough-power 1 :sort '((1 situation)))
  (declare-relation 'become-invisible 1 :sort '((1 situation)))
  (declare-relation 'can-hide 1 :sort '((1 situation)))

  (assert '(not (= spell enchanted))
          :name :spell/=enchanted)
  (assert '(not (= spell power-up))
          :name :spell/=power-up)
  (assert '(not (= enchanted power-up))
          :name :enchanted/=power-up)

  (assert '(forall ((?s :sort situation))
            (implies (enough-power ?s) (aspect1 ?s enchanted)))
          :name :enough-power=>enchanted)
  (assert '(forall ((?s :sort situation))
            (implies (not (enough-power ?s)) (aspect1 ?s spell)))
          :name :not-enough-power=>spell)

  (assert '(forall ((?s :sort situation))
            (implies (and (become-invisible ?s) (can-hide ?s))
             (aspect2 ?s enchanted)))
          :name :become-invisible/can-hide=>enchanted)
  (assert '(forall ((?s :sort situation))
            (implies (and (become-invisible ?s) (not (can-hide ?s)))
             (aspect2 ?s power-up)))
          :name :become-invisible/not-can-hide=>power-up)
  (assert '(forall ((?s :sort situation))
            (implies (not (become-invisible ?s)) (aspect2 ?s spell)))
          :name :not-become-invisible=>spell)

  (assert '(forall ((?s :sort situation) (?state :sort state))
	    (implies (and (enough-power ?s) (aspect1 ?s ?state))
	     (= ?state enchanted))))
  (assert '(forall ((?s :sort situation) (?state :sort state))
	    (implies (and (not (enough-power ?s)) (aspect1 ?s ?state))
	     (= ?state spell))))
  (assert '(forall ((?s :sort situation) (?state :sort state))
	    (implies (and (become-invisible ?s) (aspect2 ?s ?state))
	     (or (= ?state enchanted) (= ?state power-up)))))
  (assert '(forall ((?s :sort situation) (?state :sort state))
	    (implies (and (become-invisible ?s) (can-hide ?s) (aspect2 ?s ?state))
	     (= ?state enchanted))))
  (assert '(forall ((?s :sort situation) (?state :sort state))
	    (implies (and (become-invisible ?s) (not (can-hide ?s)) (aspect2 ?s ?state))
	     (= ?state power-up))))
  (assert '(forall ((?s :sort situation) (?state :sort state))
	    (implies (and (not (become-invisible ?s)) (aspect2 ?s ?state))
	     (= ?state spell)))))


(defun set-up-undecidable-theory-0 ()
  "Set up a theory in which a wizard with enough power can always try
to become invisible and hide, but we have no information if she does
not have enough power."
  (set-up-base-theory)
  (assert '(forall ((?s :sort situation))
            (implies (enough-power ?s)
             (and (become-invisible ?s) (can-hide ?s))))))

(defun set-up-undecidable-theory-1 ()
  "Set up a theory in which a wizard who doesn't have enough power
will never try to become invisible.  We cannot determine what happens
when she has enough power."
  (set-up-base-theory)
  (assert '(forall ((?s :sort situation))
            (implies (not (enough-power ?s)) (not (become-invisible ?s))))))

(defun set-up-inconsistent-theory-0 ()
  "Set up a theory in which the wizard has enough power and does not
try to become invisible.  This theory is necessarily inconsistent
since the first aspect will try to go to state `enchanted` and the
second one to `spell`."
  (set-up-base-theory)
  (declare-constant 's :sort 'situation)
  (assert '(and (enough-power s) (not (become-invisible s)))))

(defun set-up-inconsistent-theory-1 ()
  "Set up a theory in which the wizard has enough power, tries to
become invisible and cannot hide.  This theory is inconsistent."
  (set-up-base-theory)
  (declare-constant 's :sort 'situation)
  (assert '(and (enough-power s)  (become-invisible s)
            (not (can-hide s)))))

(defun set-up-inconsistent-theory-2 ()
  "Set up a theory in which the wizard has enough power and cannot
hide whenever she tries to become invisible.  This theory is
necessarily inconsistent."
  (set-up-base-theory)
  (declare-constant 's :sort 'situation)
  (assert '(enough-power s))
  (assert '(forall ((?s :sort situation))
            (implies (become-invisible ?s) (not (can-hide ?s))))))

(defun set-up-inconsistent-theory-3 ()
  "Set up a theory in which the wizard has enough power but tries not
to become invisible.  This theory is necessarily inconsistent."
  (set-up-base-theory)
  (declare-constant 's :sort 'situation)
  (assert '(and (enough-power s) (not (become-invisible s)))))

(defun set-up-inconsistent-theory-4 ()
  "Set up a theory in which the wizard has enough power but cannot
hide.  This theory is necessarily inconsistent."
  (set-up-base-theory)
  (declare-constant 's :sort 'situation)
  (assert '(and (enough-power s) (not (can-hide s)))))

(defun set-up-consistent-theory-0 ()
  "Set up a theory in which the wizard has enough power, tries to
become invisible and can hide.  This theory is consistent."
  (set-up-base-theory)
  (assert '(forall ((?s :sort situation))
            (and (enough-power ?s)
             (become-invisible ?s)
             (can-hide ?s)))))

(defun set-up-consistent-theory-1 ()
  "Set up a theory in which the wizard tries to become invisible if
and only if she has enough power, and can always hide.  This theory is
consistent."
  (set-up-base-theory)
  (assert '(forall ((?s :sort situation)) (iff (enough-power ?s) (become-invisible ?s))))
  (assert '(forall ((?s :sort situation)) (can-hide ?s))))

(defun set-up-consistent-theory-2 ()
  "Set up a theory in which the wizard with enough power tries to
become invisible and can hide, but in which a wizard that does not
have enough power does not try to become invisible.  This theory is
consistent."
  (set-up-base-theory)
  (assert '(forall ((?s :sort situation))
            (implies (enough-power ?s)
             (and (become-invisible ?s) (can-hide ?s)))))
  (assert '(forall ((?s :sort situation))
            (implies (not (enough-power ?s))
             (not (become-invisible ?s))))))

(defparameter *theories* '(:b :u0 :u1 :c0 :c1 :c2 :i0 :i1 :i2 :i3 :i4))

(defun init (&optional (theory :c1))
  (set-up-snark)
  (ecase theory
    ((:b) (set-up-base-theory))
    ((:c0) (set-up-consistent-theory-0))
    ((:c1) (set-up-consistent-theory-1))
    ((:c2) (set-up-consistent-theory-2))
    ((:i0) (set-up-inconsistent-theory-0))
    ((:i1) (set-up-inconsistent-theory-1))
    ((:i2) (set-up-inconsistent-theory-2))
    ((:i3) (set-up-inconsistent-theory-3))
    ((:i4) (set-up-inconsistent-theory-4))
    ((:u0) (set-up-undecidable-theory-0))
    ((:u1) (set-up-undecidable-theory-1))))

(defun check-inconsistent (&optional (theory :c1))
  (case theory
    (:all (check-inconsistent-all *theories* 'check-inconsistent))
    (otherwise
     (init theory)
     (prove '(exists (?s1.state ?s2.state ?s.situation)
              (and (aspect1 ?s.situation ?s1.state)
               (aspect2 ?s.situation ?s2.state)
               (not (= ?s1.state ?s2.state))))
            :answer '(incompatible-targets ?s1.state ?s2.state)))))

(defun check-inconsistent-all (&optional (theories *theories*)
                                         (check-inconsistent 'check-inconsistent))
  (let ((*print-rows-when-derived* nil)
        (*print-rows-when-finished* nil)
        (*print-agenda-when-finished* nil)
        (*print-final-rows* nil))
    (dolist (th theories)
      (format t "~&~2A: ~A~20T~A" th (funcall check-inconsistent th) (answer)))))


(defun check-consistent (&optional (theory :c1))
  (case theory
    (:all (check-consistent-all))
    (otherwise
     (init theory)
     (prove '(forall (?s1.state ?s.situation)
              (implies
               (aspect1 ?s.situation ?s1.state)
               (not (exists (?s2.state)
                     (and
                      (not (= ?s1.state ?s2.state))
                      (aspect2 ?s.situation ?s2.state))))))))))

(defun check-consistent-all (&optional (theories *theories*))
  (let ((*print-rows-when-derived* nil)
        (*print-rows-when-finished* nil)
        (*print-agenda-when-finished* nil)
        (*print-final-rows* nil))
    (dolist (th theories)
      (format t "~&~2A: ~A" th (check-consistent th)))))
